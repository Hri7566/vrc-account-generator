import crypto from 'node:crypto';

export class KeyGenerator {
	public static generateKey() {
		return crypto.randomUUID();
	}
}
